//si un utilisateur a l'un des rôles autorisés pour accéder à certaines routes ou ressources protégées
const verifyRoles = (...allowedRoles) => {
    // fonction interne middleware pour vérifier rôles autorisés 
    return (req ,res ,next) => {
        if (!req?.roles) return res.sendStatus(401);
        // Si req.roles n'est pas défini (c'est-à-dire null ou undefined),
        //alors la fonction renverra un code d'état HTTP 401
        const rolesArray = [...allowedRoles]//une copie du tableau allowedRoles 
        console.log(rolesArray) //affiche dans la console les rôles autorisés, 
        console.log(req.roles)// affiche dans la console les rôles présents dans l'objet req
        const result = req.roles.map(role => rolesArray.includes(role)).find(val => val ===true)
        if (!result) return res.sendStatus(401);
        //Cette fonction est un paramètre que le middleware reçoit 
        //et qui permet de passer le contrôle au middleware suivant dans la pile de traitement
        next()
    }
}

//Cette ligne utilise la méthode map pour transformer chaque rôle
// présent dans l'objet req.roles en un booléen qui indique s'il 
//est présent dans le tableau rolesArray (rôles autorisés).
// Ensuite, la méthode find est utilisée pour trouver le premier
// booléen qui est true (c'est-à-dire un rôle autorisé), et le
// résultat est stocké dans la variable result.

module.exports = verifyRoles