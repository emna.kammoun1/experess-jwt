const express = require('express');
const router = express.Router();
const employeesController = require('../../controllers/employeesController');
const ROLES_LIST = require('../../config/roles_list');
const verifyRoles = require('../../middleware/verifyRoles');

router.route('/')
    .get(employeesController.getAllEmployees) //récupérer tous les employés
    // créer un nouvel employé
    // la requête passera d'abord par le middleware verifyRoles vérifiera si l'utilisateur possède rôles autorisés
    //Admin ou Editor
    .post(verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor), employeesController.createNewEmployee)
   //mettre à jour les informations d'un employé existant
   //Admin ou Editor
   .put(verifyRoles(ROLES_LIST.Admin, ROLES_LIST.Editor), employeesController.updateEmployee)
   //supprimer un employé
   //vérifier si l'utilisateur a le rôle Admin requis pour accéder à cette ressource
   .delete(verifyRoles(ROLES_LIST.Admin), employeesController.deleteEmployee);

router.route('/:id')
    .get(employeesController.getEmployee);

module.exports = router;